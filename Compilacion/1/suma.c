// 1. Crear un código fuente en C que sume dos números enteros recibidos por
// teclado e imprima el resultado por pantalla.

#include <stdio.h>

int main()
{
    int num_1, num_2;
    printf("Ingrese el numero 1: ");
    scanf("%d", &num_1);

    printf("Ingrese el numero 2: ");
    scanf("%d", &num_2);

    printf("=> %d + %d = %d", num_1, num_2, num_1 + num_2);

    return 0;
}
