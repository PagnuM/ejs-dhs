#include <stdio.h>
#include <stdlib.h>
#include "mateo_maths.h"

void print_arr(int *x, int n)
{
    printf("[");
    for (int i = 0; i < (n - 1); i++)
        printf("%d, ", x[i]);
    printf("%d]", x[n - 1]);
}

int main()
{
    int n = 5;
    int nums_1[] = {10, 14, 3, 23, 1};
    printf("Nums_1: ");
    print_arr(nums_1, n);

    int nums_2[] = {4, 43, 2, 14, 56};
    printf("\nNums_2: ");
    print_arr(nums_2, n);

    int *sum = suma(nums_1, nums_2, n);
    printf("\nSuma: ");
    print_arr(sum, n);
    free(sum);

    int *diff = resta(nums_1, nums_2, n);
    printf("\nResta: ");
    print_arr(diff, n);
    free(diff);

    int *mult = multiplicacion(nums_1, nums_2, n);
    printf("\nMultiplicacion: ");
    print_arr(mult, n);
    free(mult);

    int max_1 = maximo(nums_1, n);
    printf("\nMaximo nums_1: %d", max_1);

    int max_2 = maximo(nums_2, n);
    printf("\nMaximo nums_2: %d", max_2);

    int min_1 = minimo(nums_1, n);
    printf("\nMinimo nums_1: %d", min_1);

    int min_2 = minimo(nums_2, n);
    printf("\nMinimo nums_2: %d", min_2);

    return 0;
}
