#include <stdlib.h>

int *suma(int x[], int y[], int n)
{
    int *r = malloc(n);
    for (int i = 0; i < n; i++)
        r[i] = x[i] + y[i];
    return r;
}

int *resta(int x[], int y[], int n)
{
    int *r = malloc(n);
    for (int i = 0; i < n; i++)
        r[i] = x[i] - y[i];
    return r;
}

int *multiplicacion(int x[], int y[], int n)
{
    int *r = malloc(n);
    for (int i = 0; i < n; i++)
        r[i] = x[i] * y[i];
    return r;
}

int maximo(int x[], int n)
{
    int max = x[0];
    for (int i = 1; i < n; i++)
        // Branchless programming! Este pibe es un crack se merece dormir mas
        max = max * (x[i] <= max) + x[i] * (x[i] > max);
    return max;
}

int minimo(int x[], int n)
{
    int min = x[0];
    for (int i = 1; i < n; i++)
        // Mas branchless programming! 10 instantaneo
        min = min * (x[i] >= min) + x[i] * (x[i] < min);
    return min;
}
