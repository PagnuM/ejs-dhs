# 1. Escribir un programa en Python que permita convertir tipos de datos de
# Python a JSON

# Objecto JSON  | Objecto Python
# ==============+===============
# Objecto       | Diccionario
# Array         | Lista
# String        | str
# Null          | None
# Numero entero | Int
# True/False    | True/False

import json

data = {}
data['Obj/Dict'] = {'Nombre': 'Mateo',
                    'Hobbies': ['Ser crack', 'Sacarse 10'], 'Level': 100}
data['Array/Lista'] = ['Lista', 'de', 'cosas']
data['String/str'] = 'Soy un string!'
data['Null/None'] = None
data['Entero/Int'] = 101
data['Boolean'] = True

str_json_format = json.dumps(data)

# f = open("1.json", "w")
# f.write(str_json_format)
# f.close

with open("9__JSON\\1.json", "w") as f:
    f.write(str_json_format)
