# 2. Agregar al programa anterior el camino inverso

# Objecto JSON  | Objecto Python
# ==============+===============
# Objecto       | Diccionario
# Array         | Lista
# String        | str
# Null          | None
# Numero entero | Int
# True/False    | True/False

import json

data = {}

with open("9__JSON\\1.json", "r") as f:
    data = json.load(f)

print(data)
