# 7. Escribir un programa en Python que lea información de texto de cuatro
# archivos distintos, la imprima por pantalla y luego la guarde en un archivo
# distinto.

txt = []

for i in range(1, 5):
    path = "8__Archivos\\text_" + str(i) + ".txt"
    f = open(path, "r")
    txt.append(f.readlines())
    txt[-1][-1] += '\n'

    # txt.append(list(map(lambda t: t.removesuffix('\n'), f.readlines())))
    # txt = list(map(lambda t: t.lower(), txt))
    f.close()

f = open("8__Archivos\\text_all.txt", "w")

for files in txt:
    for line in files:
        f.write(line)
