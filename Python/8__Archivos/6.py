# 6. Modificar el programa anterior para que imprima la frecuencia de repeti-
# ción de cada palabra en el archivo

txt = []
frequency = {}

f = open("8__Archivos\\ex_text.txt", "r")

while True:
    line = f.readline()
    if line == '':
        break
    txt += line.split(" ")
    txt[-1] = txt[-1].removesuffix('\n')

f.close()

print("Texto")
txt = list(map(lambda t: t.lower(), txt))
print(txt)
print()

for word in txt:
    frequency[word] = txt.count(word)

print("Frecuencia")
print(frequency)
print()
