# 4. Escribir un programa en Python que lea un archivo linea a linea y almacene
# dicho contenido en una lista

f = open("8__Archivos\\ex_text.txt", "r")

txt = []

while True:
    aux = f.readline()
    if aux == '':
        break
    txt.append(aux)

print(txt)

f.close()
