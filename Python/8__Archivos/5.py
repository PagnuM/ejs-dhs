# 5. Escribir un programa en Python lea el contenido de un archivo e identifique
# la palabra más larga de todo el archivo

f = open("8__Archivos\\ex_text.txt", "r")

txt = []

while True:
    aux = f.readline()
    if aux == '':
        break
    txt.append(aux.split(" "))

longest_word = ''
for line in txt:
    for word in line:
        longest_word = word if len(word) > len(longest_word) else longest_word

print('La palabra mas larga es "', longest_word, '"', sep='')

f.close()
