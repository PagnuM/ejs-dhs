# 5. Escribir un programa en Python que a partir de un string, extraiga las
# fechas con formato dd/mm/yyyy

import re

string = input("Ingrese una string: ")

pattern = re.compile("\\d{2}/\\d{2}/\\d{4}")

print(pattern.findall(string))
