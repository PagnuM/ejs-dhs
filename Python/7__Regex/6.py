# 6. Escribir un programa en Python que a partir de un string, extraiga todas
# las direcciones de mail en el mismo

import re

string = input("Ingrese una string: ")

pattern = re.compile("(\\w+[\\w\\.\\+-_]\\w+@\\w[\\w-]+(\\.\\w+)+)")

# (\w+[\w\.\+-_]\w+@\w[\w-]+(\.\w+)+)
# TODO: Preguntar porque tambien matchea a .com como un grupo solo
# 1234567890@example.com MATEO KLAN email@example-one.com

print(pattern.findall(string))
