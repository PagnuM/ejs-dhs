# 1. Escribir un programa en Python que verifique si una cadena de texto
# comienza con una palabra

import re

string = input("Ingrese una string: ")

# pattern = re.compile("^([a-zA-Z]*\\b)") # Falla con caracteres como < >

# TODO: Falla con acentos y palabras solas
pattern = re.compile("^(\\b[a-zA-Z]+(\\s|\\n))")

print("Comienza con una palabra? " + str(pattern.match(string) != None))
