# 3. Escribir un programa en Python que a partir de un string, verifique si
# todas las lineas del mismo contienen un ; al final.

import re

string = input("Ingrese una string: ")

pattern = re.compile(".*(?<!;)$")

print("Todas las lineas terminan en ;? " + str(pattern.match(string) == None))
