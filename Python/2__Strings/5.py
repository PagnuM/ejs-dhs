# 5. Recibir por teclado un string y contar la cantidad de mayúsculas, minús-
# culas y símbolos que se encuentran en la misma

import re

str = input("Ingrese un string: ")

mayus = re.findall("[A-Z]", str)
minus = re.findall("[a-z]", str)
symbols = re.findall("\W", str)

print("Mayusculas:", len(mayus))
print(mayus)
print("Minusculas:", len(minus))
print(minus)
print("Simbolos:", len(symbols))
print(symbols)
