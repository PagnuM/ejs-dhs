# 11. Detectar si una palabra ingresada es palindromo o no

str = input("Ingrese una palabra: ")

palindroma = True
i = 0
while palindroma:
    palindroma = str[i] == str[len(str) - i - 1]
    if i >= len(str) / 2:
        break
    i += 1

if palindroma:
    print("Es palindroma")
else:
    print("No es palindroma")
