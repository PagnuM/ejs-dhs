# 12. Detectar si una palabra es alfabética

str = input("Ingrese una palabra: ")
str = str.upper()

alfabetica = False
for s in str:
    if not((s >= "A" and s <= "Z") or s == " "):
        break
else:
    alfabetica = True

if alfabetica:
    print("Es alfabetica")
else:
    print("No es alfabetica")
