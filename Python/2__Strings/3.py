# 3. Modificar el programa para que reciba dos números enteros que permitan
# definir el comienzo y el fin de caracteres a extraer del string original

str = input("Ingrese un string con un numero: ")
num_comienzo = ""
num_final = ""
salir = False
comienzo = True

for i in range(len(str)):
    if str[i] >= "0" and str[i] <= "9":
        salir = True
        num_comienzo += str[i]
    elif salir:
        break

salir = False
for i in range(i, len(str)):
    if str[i] >= "0" and str[i] <= "9":
        salir = True
        num_final += str[i]
    elif salir:
        break

print(str[int(num_comienzo):int(num_final)])
