# 4. Solicitar el ingreso de un número por teclado e imprimirlo por pantalla
# invertida. Ejemplo: in: 123 - out: 321

num = int(input("Ingrese un numero: "))

print(str(num)[::-1])
