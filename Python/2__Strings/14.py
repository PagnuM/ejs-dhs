# 14. Luego de recibir por teclado un string que contenga letras y números,
# imprimir la cada número contenido en el string y la suma de los mismos

str = input("Ingrese una frase: ")

nums = []
aux = ""
encontro = False
for s in str:
    if s >= "0" and s <= "9":
        encontro = True
        aux += s
    elif encontro:
        nums.append(int(aux))
        aux = ""
        encontro = False

if aux != "":
    nums.append(int(aux))

suma = 0
for n in nums:
    suma += n
    print(n, ", suma=", suma, sep="")
