# 8. Partiendo de un string, recibir un número entero y contar cuántas palabras
# son mas largas, mas cortas o iguales que el número ingresado

str = input("Ingrese string: ")
str = str.strip().split()

cant = int(input("Ingrese un numero: "))

cant_mayores = 0
for s in str:
    cant_mayores += 1 if len(s) > cant else 0

print(cant_mayores)
