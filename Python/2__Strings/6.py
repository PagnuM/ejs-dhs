# 6. Recibir por teclado una frase de más de 80 caracteres. Luego, recibir tam-
# bién una segunda palabra. Finalmente, imprimir cuantas veces aparece la
# segunda palabra en la frase del comienzo

str = input("Ingrese una frase de mas de 80 caracteres: ")
str.upper()

if len(str) < 80:
    print("La frase tiene menos de 80 caracteres!")
    exit()

print()
pattern = input("Ingrese una palabra: ")
pattern.strip().upper()

print('\nLa palabra "', pattern, '" aparecio ',
      str.count(pattern), ' veces en su frase.', sep="")
