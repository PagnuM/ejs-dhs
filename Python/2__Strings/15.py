# 15. Diseñar un programa que permita validar que la estructura de una direc-
# ción de mail es correcta (debe tener un @ y al menos un punto). Por otro
# lado, realizar un validador de contraseñas (debe tener al menos una letra
# mayúscula, una minúscula, un número o un símbolo)

email = input("Email: ")
email = email.strip().split("@")

if len(email) != 2:
    print("Formato Email incorrecto")
    exit()

# Prefix
if len(email[0]) == 0:
    print("Formato Email incorrecto")
    exit()

# Domain
if email[1].find(".") == -1:
    print("Formato Email incorrecto")
    exit()

psw = input("Password: ")
minus = mayus = num_sim = False
for s in psw:
    if s >= "A" and s <= "Z":
        mayus = True
    elif s >= "a" and s <= "z":
        minus = True
    elif s == "." or s == "-" or s == "_" or (s >= "0" and s <= "9"):
        num_sim = True

if minus and mayus and num_sim:
    print("Todo OK")
elif not minus:
    print("Falta al menos una minuscula")
elif not mayus:
    print("Falta al menos una mayuscula")
elif not num_sim:
    print("Falta al menos un numro o un simbolo")
