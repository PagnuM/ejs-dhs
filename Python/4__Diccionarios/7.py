# 7. A partir del diccionario del apartado anterior, generar una lista con 10
# diccionarios. Cada uno de ellos representa un alumno distinto

estudiantes = [{}] * 10

i = 1
for est in estudiantes:
    print('------------- ESTUDIANTE #{} -------------'.format(i))
    i += 1

    est['Nombre'] = input('Introducza un nombre: ')
    est['Apellido'] = input('Introducza un apellido: ')
    est['Legajo'] = input('Introducza un legajo: ')

    j = 0
    mats = {}
    while True:
        j += 1
        mat = input(
            '#{} Ingrese una nueva materia (exit para parar): '.format(j))

        if mat.strip == 'exit':
            break

        mats[mat] = int(
            input('#{} Ingrese el codigo de la materia: '.format(j)))

    est['Materias'] = mats

    print('==========================================')

    print()
    print(estudiantes)
