# 5. A partir del diccionario del ejercicio anterior, modificar el número de legajo
# y agregar dos materias nuevas en la lista de materias

estudiante = {'Nombre': 'Mateo', 'Apellido': 'Pagnucco',
              'Legajo': '5823', 'Materias': ['PDS I', 'Ing Web', 'DHS']}
print(estudiante)
print()

# estudiante.update(Legajo=input('Acualize el legajo: '))
estudiante['Legajo'] = input('Acualize el legajo: ')

# l = list(estudiante.get('Materias'))
# for i in range(2):
#     l.append(input('Ingrese una nueva materia: '))
# estudiante.update(Materia=l)
estudiante['Materias'] += input('Ingrese nuevas materias: ').split(', ')

print()
print(estudiante)

# estudiante.update(Materias=estudiante['Materias'] + input(
#     'Agruegue mas materias (separadas por coma): ').split(', '))
