# 1. Crear una clase vehículo que contenga los atributos marca, velocidad máxi-
# ma y kilómetros recorridos. La marca debe ser ingresada en el constructor
# y los demás atributos deben tener sus métodos getters y setters.

class Vehiculo():
    def __init__(self, marca):
        self.marca = marca

    def set_max_vel(self, vel):
        self.__max_vel = vel

    def set_km_rec(self, km):
        self.__km_rec = km

    def get_max_vel(self):
        return (self.__max_vel)

    def get_km_rec(self):
        return (self.__km_rec)


v = Vehiculo('Renault')
v.set_km_rec(300.5)
v.set_max_vel(140.0)

print('Marca: ' + v.marca)
print('Km recorridos: ' + str(v.get_km_rec()))
print('Max vel: ' + str(v.get_max_vel()))
