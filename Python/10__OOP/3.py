# 3. Agregar infraestructura unit test al ejercicio anterior y comparar que la
# comparación funciona para vehículos más lentos, más rápidos o igual de
# veloces.

import pytest


class Vehiculo():
    def __init__(self, marca):
        self.marca = marca

    def set_max_vel(self, vel):
        self.__max_vel = vel

    def set_km_rec(self, km):
        self.__km_rec = km

    def get_max_vel(self):
        return (self.__max_vel)

    def get_km_rec(self):
        return (self.__km_rec)

    # Magic Methods
    def __eq__(self, other):
        return self.get_max_vel() == other.get_max_vel()

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        return self.get_max_vel() < other.get_max_vel()

    def __le__(self, other):
        return self.get_max_vel() <= other.get_max_vel()

    def __gt__(self, other):
        return self.get_max_vel() > other.get_max_vel()

    def __ge__(self, other):
        return self.get_max_vel() >= other.get_max_vel()


v_1 = Vehiculo('Renault')
v_1.set_km_rec(300.5)
v_1.set_max_vel(140.0)

v_2 = Vehiculo('Ford')
v_2.set_km_rec(201.5)
v_2.set_max_vel(240.0)

assert (v_1 == v_2) == False
assert (v_1 != v_2) == True
assert (v_1 > v_2) == False
assert (v_1 >= v_2) == False
assert (v_1 < v_2) == True
assert (v_1 <= v_2) == True
