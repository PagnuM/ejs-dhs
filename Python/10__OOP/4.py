# 4. Definir una clase abstracta llamada vehículo con las interfaces arrancar(),
# frenar() y tiempo_de_marcha() Luego implementar las clases hijas:
# a) Colectivo
# b) Motocicleta
# c) Camión
# d) Automóvil
# Implementar cada uno de los métodos heredados de la clase abstracta.
# Luego, crear una lista con 10 instancias Colectivo, 2 motocicleta, 3 ca -
# miones y 3 automóviles. Se debe recorrer la lista, invocar a los métodos
# arrancar y esperar un tiempo random.
# Finalmente, se debe volver a recorrer la lista, invocando los métodos frenar
# e imprimir el tiempo de marcha de cada uno de los vehículos(diferencia
# de tiempos entre arrancar y frenar).

from abc import ABC, abstractmethod


class Vehiculo(ABC):

    @abstractmethod
    def __init__(self, vel):
        self.vel = vel
        self.m_rec = 0
        self.prendido = False

    @abstractmethod
    def arrancar(self):
        pass

    @abstractmethod
    def frenar(self):
        pass

    @abstractmethod
    def tiempo_de_marcha(self):
        pass


class Colectivo(Vehiculo):
    def __init__(self, vel):
        super().__init__(vel)

    def arrancar(self):
        self.prendido = True

    def frenar(self):
        self.prendido = True

    def tiempo_de_marcha(self):
        self.m_rec += self.vel

# TODO: No entiendo que mas hacer
