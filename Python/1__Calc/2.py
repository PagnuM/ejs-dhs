# 2. Realizar un programa que calcule el desglose mínimo en billetes y monedas
# de una cantidad exacta de pesos. Hay billetes de $1000, $500, $200, $100 y
# $500 y monedas de $10, $5 y $2 y $1. El desglose se realiza del más grande
# al más pequeño. . El desglose se realiza del más grande al más pequeño.

billetes = [1000, 500, 200, 100]
monedas = [10, 5, 2, 1]
denominaciones = billetes + monedas

desglose = []

dinero = int(input("Ingrese una cantidad de dinero: "))
dinero_tmp = dinero

i = 0

while dinero_tmp > 0:
    if(dinero_tmp >= denominaciones[i]):
        desglose.append(denominaciones[i])
        dinero_tmp -= denominaciones[i]
    else:
        i += 1

print(desglose)
