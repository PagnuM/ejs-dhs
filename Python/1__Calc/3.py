# 3. Diseñar un programa que, dados cinco puntos en el plano, determine cuál
# de los cuatro últimos es más cercano al primero.

x = float(input("Punto 1, x: "))
y = float(input("Punto 1, y: "))
pares = [(x, y)]
distancia = [0]

for i in range(1, 5):
    x = float(input("\nPunto {}, x: ".format(i+1)))
    y = float(input("Punto {}, y: ".format(i+1)))
    pares.append((x, y))
    distancia.append(((pares[i][0] - pares[0][0])
                     ** 2 + (pares[i][1] - pares[0][1])**2)**(1/2))

i_menor = 1
for i in range(2, len(distancia)):
    i_menor = i if distancia[i] < distancia[i_menor] else i_menor

print("\nEl punto mas cercano es el #{}, osea el punto {}".format(
    i_menor + 1, pares[i_menor]))
