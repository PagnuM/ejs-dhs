# 4. Dado un rango de 10 números enteros, iterar desde el primero al último y
# en cada iteración imprimir:
# * Valor actual
# * Valor anterior
# * Valor siguiente
# * Suma acumulada
# Nota: se debe usar range() para generar el rango de números

nums = []
suma = 0

for i in range(10):
    nums.append(int(input("Ingrese el numero {}: ".format(i + 1))))

for i in range(10):
    suma += nums[i]
    print()

    if i != 0:
        print("Valor anterior:", nums[i - 1])

    print("Valor actual:", nums[i])

    if i < 9:
        print("Valor siguiente:", nums[i + 1])

    print("Suma acumulada", suma)
