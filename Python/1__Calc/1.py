# 1. Dado dos números enteros imprimir por pantalla el producto de los mismos.
# Si el producto es más grande que 10000, imprimir su suma

num1 = float(input("Ingrese numero 1: "))
num2 = float(input("Ingrese numero 2: "))

if(num1*num2 <= 10000):
    print("Num1 * Num2 = ", num1*num2)
else:
    print("Num1 + Num2 = ", num1+num2)
