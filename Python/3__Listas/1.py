# 1. Considerando dos listas de números enteros:
# De cada una de las listas imprimir los números divisibles por 5
# * Calcular e imprimir la suma vectorial de ambas
# * Calcular e imprimir el producto punto
# * De cada una de las listas imprimir los números divisibles por 5

l_1 = [8, 3, 15, 29, 5, 100, 150, 12]
l_2 = [85, 82, 10, 4, 25, 12, 90, 7]

suma_vec = []
prod = []

for i in range(len(l_1)):
    suma_vec.append(l_1[i] + l_2[i])
    prod.append(l_1[i] * l_2[i])

print("Lista 1")
print("Divisibles por 5:", end="")
for n in l_1:
    if n % 5 == 0:
        print(n, sep=" ", end="")
print()
print()

print("Lista 2")
print("Divisibles por 5:", end="")
for n in l_2:
    if n % 5 == 0:
        print(n, sep=" ", end="")
print()
print()

print("Suma vectorial")
print(l_1)
print(" + ")
print(l_2)
print("="*(max(len(str(l_1)), len(str(l_2)))))
print(suma_vec)

print()
print("Producto Punto")
print(l_1)
print(" * ")
print(l_2)
print("="*(max(len(str(l_1)), len(str(l_2)))))

prod_punto = 0
for p in prod:
    prod_punto += p

print(prod, "=", prod_punto)
