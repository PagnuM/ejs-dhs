# 4. Diseñar un programa que dada una lista con números positivos y negativos,
# sustituya los negativos por cero

import random

l = []
for n in range(10):
    l.append(random.randint(-100, 100))

print(l)

for i in range(len(l)):
    if l[i] < 0:
        l[i] = 0

print(l)
