# 2. Crear una lista con 100 números aleatorios, luego se debe generar dos listas
# más: una que contenga los números pares de la primera y otra los impares

import random

l = []
pares = []
impares = []
for n in range(100):
    l.append(random.randint(0, 1000))

for n in l:
    if n % 2 == 0:
        pares.append(n)
    else:
        impares.append(n)

print("Pares:")
print(pares)
print()

print("Impares:")
print(impares)