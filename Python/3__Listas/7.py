# 7. Crear una lista que contenga en su interior 27 listas, asociadas cada una
# a una letra del alfabeto español.
# Luego tomar por teclado 32 palabras y añadirlas cada una a la lista co-
# rrespondiente a su primera letra.

palabras = [[]for i in range(27)]

for i in range(32):
    str = input("Ingrese la frase #{}/32:".format(i + 1))
    palabras[ord(str[0].lower()) - ord("a")].append(str)

print(palabras)
