# 6. Diseñar un programa que tomando una lista de números enteros, genere
# una nueva eliminando los números repetidos de la primera

lst = [1, 5, 3, 2, 6, 5, 3, 5, 1]

for elem in lst:
    for i in range(lst.count(elem) - 1):
        lst.remove(elem)

print(lst)
