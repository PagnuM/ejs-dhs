import cmath
from PIL import Image, ImageDraw


def map_from_to(x, a, b, c, d) -> float:
    y = (x-a)/(b-a)*(d-c)+c
    return y


def mandelbrot(c: complex):
    max_iter = 500
    z_v = 0

    for i in range(max_iter):
        try:
            z_n = z_v*z_v + c

            # ! Sale mal el plot cuando activo el if
            # if abs(z_n) < 2:
            #     return True
        except OverflowError:
            return False

        z_v = z_n

    return abs(z_v) < 2


size = (800, 600)
img = Image.new(mode='RGB', size=size)
draw = ImageDraw.Draw(img)

# x, y = 0, 0

for x in range(size[0]):
    for y in range(size[1]):
        x_m = map_from_to(x, 0, size[0], -2, 1)
        y_m = map_from_to(y, 0, size[1], -1, 1)
        z = complex(x_m, y_m)

        color = (0, 0, 0) if mandelbrot(z) else (255, 255, 255)
        draw.point((x, y), fill=color)

# Guardar
img.save("6__Mandelbrot\\plot.jpeg", "JPEG")
