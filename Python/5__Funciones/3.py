# 3. Escribir una función que reciba un string y luego calcule y retorne la
# cantidad de letras mayúsculas y minúsculas presentes en la misma

def may_min(string: str):
    may = min = 0
    for s in string:
        may += 1 if s.isupper() else 0
        min += 1 if s.islower() else 0
    return may, min


string = input("Ingrese un string: ")
may, min = may_min(string)
print("Su string tiene {} mayusculas y {} minusculas".format(may, min))
