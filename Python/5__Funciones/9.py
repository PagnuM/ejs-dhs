# 9. Crear una lista de diez diccionarios donde cada uno de ellos contenga la
# información de un developer
# * Nombre
# * Edad
# * Lenguajes manejados
# * Años de experiencia
# Se debe imprimir la lista de todas las formas mencionadas a continuación.
# Cada una de ellas debe ser implementada con una función lambda distinta.
# * Alfabéticamente por nombre
# * Edad
# * Años de experiencia en forma ascendente
# * Años de experiencia en forma descendente

developers = []
developers.append({'Nombre': 'Mateo', 'Edad': 21, 'Lenguajes': [
                  'C++', 'Java', 'Python', 'JS'], 'Exp': 0})
developers.append({'Nombre': 'Juancito', 'Edad': 34, 'Lenguajes': [
                  'C++', 'Python', 'JS'], 'Exp': 12})
developers.append({'Nombre': 'Santi', 'Edad': 12,
                  'Lenguajes': ['Scratch'], 'Exp': 0})
developers.append({'Nombre': 'Carlos', 'Edad': 25, 'Lenguajes': [
                  'Java', 'Python', 'JS', 'Scala', 'Haskell'], 'Exp': 4})
developers.append({'Nombre': 'Juan', 'Edad': 48, 'Lenguajes': [
                  'Java', 'Python', 'JS', 'FORTRAN'], 'Exp': 22})

# alf_nombre = developers.copy()
# alf_nombre.sort(key=(lambda d: d.get('Nombre')))
alf_nombre = sorted(developers, key=(lambda d: d['Nombre']))
print('Por nombre:', alf_nombre)
print()

edad = sorted(developers, key=lambda d: d['Edad'])
print('Edad:', edad)
print()

exp_asc = sorted(developers, key=lambda d: d['Exp'])
print('Exp Asc:', exp_asc)
print()

exp_des = sorted(developers, key=lambda d: d['Exp'], reverse=True)
print('Exp Des:', exp_des)
print()
