# 8. Escribir un programa que partiendo de una lista de números enteros, los
# clasifique en dos listas distintas: una con los números pares y otra con los
# impares. Resolver utilizando lambdas

l = [1, 3, 5, 6, 2, 1, 2, 10, 11]

pares = list(filter(lambda i: i % 2 == 0, l))
impares = list(filter(lambda i: i % 2 != 0, l))

print("Pares:", pares)
print("Impares:", impares)
