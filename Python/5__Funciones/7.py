# 7. Luego de instalar packete externo time utilizando Anaconda (link a docu-
# mentación), crear un decorador que permita medir el tiempo de ejecución
# de una función.
# Luego, se debe implementar una función que permita multiplicar elemento
# a elemento los siguientes objetos:
# * Dos listas del 100.000 elementos enteros
# * Dos tuplas del 100.000 elementos enteros
# * Dos listas del 100.000 elementos flotantes
# * Dos tuplas del 100.000 elementos flotantes

import time
import random


def medir_tiempo(function):
    def funcion_a_devolver(arg_1, arg_2):
        inicio = time.time_ns()
        r = function(arg_1, arg_2)
        final = time.time_ns()
        print('Delta T: ', (final - inicio)/(1000000), 'ms', sep='')
        return r

    return funcion_a_devolver


@medir_tiempo
def mult(c_1, c_2):
    return [c_1[i]*c_2[i] for i in range(len(c_1))]


l_int_1 = [random.randint(-500, 500) for _ in range(100000)]
l_int_2 = [random.randint(-500, 500) for _ in range(100000)]

t_int_1 = tuple(random.randint(-500, 500) for _ in range(100000))
t_int_2 = tuple(random.randint(-500, 500) for _ in range(100000))

l_float_1 = [random.uniform(-500, 500) for _ in range(100000)]
l_float_2 = [random.uniform(-500, 500) for _ in range(100000)]

t_float_1 = tuple(random.uniform(-500, 500) for _ in range(100000))
t_float_2 = tuple(random.uniform(-500, 500) for _ in range(100000))

print('='*27)

print('Listas enteros, ', end='')
l_int = mult(l_int_1, l_int_2)
print('Tuplas enteros, ', end='')
t_int = tuple(mult(t_int_1, t_int_2))

print('Listas flotantes, ', end='')
l_float = mult(l_float_1, l_float_2)
print('Tuplas flotantes, ', end='')
t_float = tuple(mult(t_float_1, t_float_2))
