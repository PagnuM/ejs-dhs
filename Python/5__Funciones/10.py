# 10. Diseñar un programa que sume tres listas. Utilizar map() y lambdas.

l_1 = [1, 3, 5, 3]
l_2 = [20, 3, 9, 33]
l_3 = [9, 3, 2, 5]

suma = list(map(lambda x, y, z: x + y + z, l_1, l_2, l_3))
print(suma)
