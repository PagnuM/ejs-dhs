# 2. Crear una función que imprima las primeras n filas de un triángulo de
# Pascal.

def pascal(row, n):
    if n == 1:
        print([1])
        return [1]

    fila = []
    row = pascal(row, n - 1)
    for i in range(0, n - 2):
        fila.append(row[i] + row[i + 1])
    r = [1] + fila + [1]
    print(r)
    return r


n = 10
pascal([], n)
