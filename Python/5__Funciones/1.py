# 1. Crear una función en Python que imprima el valor y el ID de un objeto
# recibido en la misma. Luego crear un objeto lista, un entero y una tupla.
# Finalmente se deben imprimir estos tres objetos utilizando la función y
# sin hacerlo.

# Extraer conclusiones sobre como las funciones en Python reciben paráme-
# tros.

def detalles(nombre, obj):
    print("###", nombre, "###")
    print("valor:", obj)
    print("id:", id(obj))
    print("#"*(len(nombre) + 8))


lst = ['a', 'b']
entero = 10
tupla = ("Mateo", 21)

detalles("Lista", lst)
print("valor:", lst)
print("id:", id(lst))
print()

detalles("Entero", entero)
print("valor:", entero)
print("id:", id(entero))
print()

detalles("Tupla", tupla)
print("valor:", tupla)
print("id:", id(tupla))
print()

'''
Se pasan inicialmente por referencia (referencian al mismo lugar en memoria)
pero cuando las variables cambian dentro de la funcion se crea un nuevo objeto
'''
