# 13. Diseñar un programa que a partir de una lista genere otra con sólo los
# números positivos. Utilizar filter()

l = [-3, 1, 4, -2, -123, 32, 55, -2]

pos = list(filter(lambda x: x >= 0, l))
print(pos)
