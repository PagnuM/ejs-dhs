# 11. Diseñar un programa calcule la raíz cuadrada de cada elemento de una
# lista. Utilizar map().

l = [4, 2, 9, 16, 45, 2.4]
r = list(map(lambda x: x**(1/2), l))

print(r)
