# 6. Luego de instalar packete externo colour utilizando Anaconda (link a do-
# cumentación), crear una función que permita imprimir un mensaje por
# consola. Se debe decorar la misma para permitir su utilización mediante
# tres formas:
# * Impresión de warnings: letras escritas en amarillo
# * Impresión de mensajes críticos: letras escritas en rojo
# * Impresión de normal: letras escritas en blanco

from colour import Color


def print_to_console(str, color):
    print(str)


str = 'Hola Mundo!'

amarillo = Color("yellow")
rojo = Color("red")
blanco = Color("white")

# TODO: No encuentro como imprimir con color usando los colores del colour
