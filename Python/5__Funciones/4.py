# 4. Escribir una función que reciba una lista y retorne una nueva lista con los
# elementos de la primera sin repeticiones

# TODO: Preguntar explicaciones sobre estas soluciones https://stackoverflow.com/a/41523055

def sin_rep(lst: list):
    for elem in lst:
        for i in range(lst.count(elem) - 1):
            lst.remove(elem)
    return lst


lst = [1, 5, 3, 2, 6, 5, 3, 5, 1]
print(sin_rep(lst))
